# Algorithms 
## Description
Solved problems of "Algorithms" course by Robert Sedgewick and Kevin Wayne (Princeton University) published on Coursera 

## Programming assignment list
### Part I
* Percolation
* Deques and Randomized Queues
* Collinear Points
* 8 Puzzle
* Kd-Trees
### Part II
* WordNet
* Seam Carving
* Baseball Elimination
* Boggle
* Burrows-Wheeler

## Course overview
### Part I
* Union-Find
* Analysis of Algorithms
* Stacks and Queues
* Elementary Sorts, MergeSort, QuickSort
* Priority Queues
* Elementary Symbol Tables
* Balanced Search Trees
* Geometric Applications of BSTs
* Hash Tables
* Symbol Table Applications
### Part II
* Undirected and Directed Graphs
* Minimum Spanning Trees
* Shortest Path
* Maximum Flow and Minimum Cut
* Radix Sort
* Tries
* Substring Search
* Regular Expressions
* Data Compression
* Reductions
* Linear Programming
* Intractability