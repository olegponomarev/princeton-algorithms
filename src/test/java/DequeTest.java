import org.junit.Assert;
import org.junit.Test;

/**
 * @Author Oleg Ponomarev
 * @since 21.01.2017
 */
public class DequeTest {
    @Test
    public void dequeTest() {
        Deque<String> stringDeque = new Deque <>();
        Assert.assertEquals(0, stringDeque.size());
        stringDeque.addLast("last");
        Assert.assertEquals(1, stringDeque.size());
        Assert.assertEquals("last", stringDeque.removeFirst());
        Assert.assertEquals(0, stringDeque.size());
        Assert.assertTrue(stringDeque.isEmpty());
        stringDeque.addFirst("2");
        stringDeque.addLast("3");
        stringDeque.addFirst("1");
        stringDeque.addLast("4");
        int i = 1;
        for (String s : stringDeque) {
            Assert.assertEquals(String.valueOf(i++), s);
        }
        int external = 1;
        for (String s : stringDeque) {
            int internal = 1;
            for (String s1 : stringDeque) {
                Assert.assertEquals(String.valueOf(internal++), s1);
            }
            Assert.assertEquals(String.valueOf(external++), s);
        }
    }

    @Test
    public void doublyLinked() {
        Deque<String> stringDeque = new Deque <>();
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.addFirst("1");
        stringDeque.removeLast();
        stringDeque.removeLast();
        Assert.assertEquals(5, stringDeque.size());
    }

    @Test
    public void isEmpty() {
        Deque<String> stringDeque = new Deque <>();
        Assert.assertTrue(stringDeque.isEmpty());
        stringDeque.addLast("1");
        stringDeque.removeLast();
        Assert.assertTrue(stringDeque.isEmpty());
    }

    @Test
    public void isEmpty1() {
        Deque<Integer> deque = new Deque <>();
        deque.addLast(0);
        deque.addLast(1);
        deque.removeFirst();//     ==> 0
        deque.removeLast();//      ==> 1
        deque.addFirst(4);
        deque.removeFirst();//     ==> 4
        deque.size();
        deque.addLast(7);
        deque.removeLast();//      ==> 7
        deque.size();
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void iteratorTest() {
        Deque<Integer> deque = new Deque <>();
        deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);
        deque.addFirst(4);
        deque.addFirst(5);
        deque.addLast(6);
        deque.addLast(7);
        deque.removeFirst();//   ==> 5
        deque.addFirst(9);
        deque.removeLast();//    ==> 7
        int count = 0;
        for (Integer integer : deque) {
//            System.out.print(integer + " ");
            count++;
        }
        Assert.assertEquals(6, count);
    }
}
