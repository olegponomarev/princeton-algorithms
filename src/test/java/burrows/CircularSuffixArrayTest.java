package burrows;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircularSuffixArrayTest {
    @Test
    public void index() throws Exception {
        assertEquals(3, new CircularSuffixArray("BBBAAABBBA").index(0));
        assertEquals(6, new CircularSuffixArray("JRXKUTIBLI").index(1));
        assertEquals(6, new CircularSuffixArray("CADABRA!ABRA").index(1));
    }

}