package wordnet;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Topological;

import java.util.*;

public class WordNet {
    private final Map<Integer, List<String>> synsets;
    private final SAP sap;
    private final Set<String> nouns;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) {
            throw new IllegalArgumentException();
        }
        List<String> lines = new ArrayList<>();
        In in = new In(hypernyms);
        String s;
        while ((s = in.readLine()) != null) {
            lines.add(s);
        }
        in.close();
        Digraph digraph = new Digraph(lines.size() + 1);
        for (String line : lines) {
            String[] words = line.split(",");
            for (int i = 1; i < words.length; i++) {
                digraph.addEdge(Integer.parseInt(words[0]), Integer.parseInt(words[i]));
            }
        }
        sap = new SAP(digraph);
        Topological topological = new Topological(digraph);
        if (!topological.hasOrder()) {
            throw new IllegalArgumentException();
        }
        this.synsets = new HashMap<>();
        in = new In(synsets);
        nouns = new HashSet<>();
        String line;
        while ((line = in.readLine()) != null) {
            String[] words = line.split(",");
            final List<String> strings = Arrays.asList(words[1].split(" "));
            this.synsets.put(Integer.valueOf(words[0]), strings);
            nouns.addAll(strings);
        }
        in.close();
    }

    public Iterable<String> nouns() {
        return new HashSet<>(nouns);
    }

    public boolean isNoun(String word) {
        if (word == null) {
            throw new IllegalArgumentException();
        }
        return nouns.contains(word);
    }

    public int distance(String nounA, String nounB) {
        if (nounA == null || nounB == null || !isNoun(nounA) || !isNoun(nounB)) {
            throw new IllegalArgumentException();
        }
        if (nounA.equals(nounB)) {
            return 0;
        }
        List<Integer> first = new ArrayList<>();
        List<Integer> second = new ArrayList<>();
        for (Map.Entry<Integer, List<String>> entry : synsets.entrySet()) {
            if (entry.getValue().contains(nounA)) {
                first.add(entry.getKey());
            }
            if (entry.getValue().contains(nounB)) {
                second.add(entry.getKey());
            }
        }
        return sap.length(first, second);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path
    public String sap(String nounA, String nounB) {
        if (nounA == null || nounB == null || !isNoun(nounA) || !isNoun(nounB)) {
            throw new IllegalArgumentException();
        }
        List<Integer> first = new ArrayList<>();
        List<Integer> second = new ArrayList<>();
        for (Map.Entry<Integer, List<String>> entry : synsets.entrySet()) {
            if (entry.getValue().contains(nounA)) {
                first.add(entry.getKey());
            }
            if (entry.getValue().contains(nounB)) {
                second.add(entry.getKey());
            }
        }
        int ancestor = sap.ancestor(first, second);
        StringBuilder stringBuilder = new StringBuilder();
        for (String part : synsets.get(ancestor)){
            stringBuilder.append(part).append(" ");
        }
        return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
    }

    public static void main(String[] args) {
        WordNet wordNet = new WordNet(args[0], args[1]);
        System.out.println(wordNet.distance("a", "n"));
    }
}
