package wordnet;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {
    private final WordNet wordNet;
    public Outcast(WordNet wordnet) {
        this.wordNet = wordnet;
    }
    public String outcast(String[] nouns) {
        int maxSum = Integer.MIN_VALUE;
        int maxIndex = 0;
        for (int i = 0; i < nouns.length; i++) {
            int sum = 0;
            for (String noun : nouns) {
                sum += wordNet.distance(nouns[i], noun);
            }
            if (sum > maxSum) {
                maxSum = sum;
                maxIndex = i;
            }
        }
        return nouns[maxIndex];
    }
    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }
}
