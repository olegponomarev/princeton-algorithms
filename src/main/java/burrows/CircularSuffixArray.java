package burrows;

import java.util.Arrays;
import java.util.Comparator;

public class CircularSuffixArray {
    private final int length;
    private final Integer[] indices;

    public CircularSuffixArray(String s) {
        if (s == null) {
            throw new IllegalArgumentException();
        }
        this.length = s.length();
        indices = new Integer[s.length()];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = i;
        }
        Arrays.sort(indices, comparator(s));
    }

    private Comparator<Integer> comparator(String s) {
        return (o1, o2) -> {
            for (int i = 0; i < s.length(); i++) {
                int diff = s.charAt((o1 + i) % s.length()) - s.charAt((o2 + i) % s.length());
                if (diff != 0) {
                    return diff;
                }
            }
            return 0;
        };

    }

    public int length() {
        return length;
    }

    public int index(int i) {
        if (i < 0 || i >= length) {
            throw new IllegalArgumentException();
        }
        return indices[i];
    }

    public static void main(String[] args) {
        CircularSuffixArray array = new CircularSuffixArray("ABRACADABRA!");
        System.out.println("length: " + array.length);
        for (int i = 0; i < array.length; i++) {
            System.out.println(array.index(i));
        }
    }
}
