package burrows;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {
    public static void encode() {
        char[] array = new char[256];
        for (char i = 0; i < array.length; i++) {
            array[i] = i;
        }
        final String string = BinaryStdIn.readString();
        for (int i = 0; i < string.length(); i++) {
            final char c = string.charAt(i);
            final char position = currentPosition(c, array);
            BinaryStdOut.write(position);
            System.arraycopy(array, 0, array, 1, position);
            array[0] = c;
        }
        BinaryStdOut.close();
    }

    private static char currentPosition(char c, char[] chars) {
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == c) {
                return (char) i;
            }
        }
        throw new RuntimeException();
    }

    public static void decode() {
        char[] array = new char[256];
        for (char i = 0; i < array.length; i++) {
            array[i] = i;
        }
        while (!BinaryStdIn.isEmpty()) {
            final char c = BinaryStdIn.readChar();
            final char x = array[c];
            BinaryStdOut.write(x);
            System.arraycopy(array, 0, array, 1, c);
            array[0] = x;
        }
        BinaryStdOut.close();
    }

    public static void main(String[] args) {
        if (args[0].equals("-")) {
            encode();
        } else if (args[0].equals("+")) {
            decode();
        } else {
            throw new RuntimeException();
        }
    }
}
