package burrows;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.Arrays;

public class BurrowsWheeler {
    public static void transform() {
        final String string = BinaryStdIn.readString();
        CircularSuffixArray array = new CircularSuffixArray(string);
        int i = 0;
        while (array.index(i) != 0) {
            i++;
        }
        BinaryStdOut.write(i);
        for (int j = 0; j < array.length(); j++) {
            BinaryStdOut.write(string.charAt((string.length() - 1 + array.index(j)) % string.length()));
        }
        BinaryStdOut.close();
    }

    public static void inverseTransform() {
        int first = BinaryStdIn.readInt();
        char[] transformed = BinaryStdIn.readString().toCharArray();
        int[] next = getNext(transformed);
        int current = first;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < transformed.length; i++) {
            sb.append(transformed[current]);
            current = next[current];
        }
        BinaryStdOut.write(sb.toString());
        BinaryStdOut.close();
    }

    private static int[] getNext(char[] sorted) {
        char[] transformed = Arrays.copyOf(sorted, sorted.length);
        Arrays.sort(sorted);
        final int[] result = new int[transformed.length];
        boolean[] used = new boolean[transformed.length];
        for (int i = 0; i < transformed.length; i++) {
            char c = transformed[i];
            int index = Arrays.binarySearch(sorted, c);
            while (index > 0 && sorted[index - 1] == c && !used[index - 1]) {
                index--;
            }
            while (used[index]) {
                index++;
            }
            result[index] = i;
            used[index] = true;
        }
        return result;
    }

    public static void main(String[] args) {
        if (args[0].equals("-")) {
            transform();
        } else if (args[0].equals("+")) {
            inverseTransform();
        } else {
            throw new RuntimeException();
        }
    }
}
