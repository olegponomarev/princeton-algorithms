import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

/**
 *  The 8-puzzle problem is a puzzle invented and popularized by Noyes Palmer Chapman in the 1870s.
 *  It is played on a 3-by-3 grid with 8 square blocks labeled 1 through 8 and a blank square. Your goal is
 *  to rearrange the blocks so that they are in order, using as few moves as possible.
 *  You are permitted to slide blocks horizontally or vertically into the blank square.
 *
 * @author Oleg Ponomarev
 * @since 08.02.2017
 */
public final class Solver {

    private MinPQ<Node> boardMinPQ;
    private boolean isSolvable;
    private int movesToSolve;
    private Node goalNode;

    public Solver(Board initial) {
        if (initial == null) {
            throw new NullPointerException();
        }
        boardMinPQ = new MinPQ<>();
        boardMinPQ.insert(new Node(initial, 0, null));
        boardMinPQ.insert(new Node(initial.twin(), 0, null));
        while (true) {
            Node searchNode = boardMinPQ.delMin();
            if (searchNode.board.isGoal()) {
                goalNode = searchNode;
                movesToSolve = searchNode.moves;
                break;
            }
            for (Board board : searchNode.board.neighbors()) {
                if (searchNode.previous == null || !board.equals(searchNode.previous.board)) {
                    boardMinPQ.insert(new Node(board, searchNode.moves + 1, searchNode));
                }
            }
        }

        Node initialNode = goalNode;
        while (initialNode.previous != null) {
            initialNode = initialNode.previous;
        }
        if (initialNode.board.equals(initial)) {
            isSolvable = true;
        } else if (!initialNode.board.equals(initial.twin())) {
            throw new RuntimeException("A* doesn't work");
        }
    }

    private class Node implements Comparable<Node> {
        private Board board;
        private int moves;
        private int priority;
        private Node previous;

        public Node(Board board, int moves, Node previous) {
            this.board = board;
            this.moves = moves;
            this.previous = previous;
            this.priority = board.manhattan() + moves;
        }

        @Override
        public int compareTo(Node o) {
            int diff = Node.this.priority - o.priority;
            if (diff != 0) {
                return diff;
            } else {
                return Node.this.board.hamming() - o.board.hamming();
            }
        }
    }

    public boolean isSolvable() {
        return isSolvable;
    }

    public int moves() {
        if (!isSolvable()) {
            return -1;
        }
        return movesToSolve;
    }

    public Iterable<Board> solution() {
        if (!isSolvable()) {
            return null;
        }
        Stack<Board> boardStack = new Stack<>();
        boardStack.push(goalNode.board);

        Node node = goalNode;
        while (node.previous != null) {
            node = node.previous;
            boardStack.push(node.board);
        }
        return boardStack;
    }

    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
