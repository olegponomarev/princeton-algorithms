package seam;

import edu.princeton.cs.algs4.Picture;

import java.awt.Color;

public class SeamCarver {
    private static final int BORDER = 1000;
    private Color[][] pixels;
    private double[][] energies;
    private boolean transposed;
    private int currentWidth;
    private int currentHeight;

    public SeamCarver(Picture picture) {
        if (picture == null) {
            throw new IllegalArgumentException();
        }
        this.currentHeight = picture.height();
        this.currentWidth = picture.width();
        this.pixels = new Color[currentWidth][];
        for (int i = 0; i < currentWidth; i++) {
            pixels[i] = new Color[currentHeight];
            for (int j = 0; j < currentHeight; j++) {
                pixels[i][j] = picture.get(i, j);
            }
        }

        this.energies = new double[currentWidth][];
        for (int i = 0; i < pixels.length; i++) {
            energies[i] = new double[currentHeight];
            for (int j = 0; j < pixels[i].length; j++) {
                energies[i][j] = energyInternal(i, j);
            }
        }
    }

    public Picture picture() {
        if (transposed) {
            transpose();
        }
        final Picture picture = new Picture(currentWidth, currentHeight);
        for (int i = 0; i < width(); i++) {
            for (int j = 0; j < height(); j++) {
                picture.set(i, j, pixels[i][j]);
            }
        }
        return picture;
    }

    public int width() {
        return transposed ? currentHeight : currentWidth;
    }

    public int height() {
        return transposed ? currentWidth : currentHeight;
    }

    public double energy(int x, int y) {
        if (transposed) {
            return energyInternal(y, x);
        } else {
            return energyInternal(x, y);
        }
    }

    private double energyInternal(int x, int y) {
        if (x < 0 || x >= currentWidth || y < 0 || y >= currentHeight) {
            throw new IllegalArgumentException();
        }
        if (x == 0 || y == 0 || x == currentWidth - 1 || y == currentHeight - 1) {
            return BORDER;
        }
        return StrictMath.sqrt(deltaSquarePart(x, y, true) + deltaSquarePart(x, y, false));
    }

    public int[] findHorizontalSeam() {
        return find(false);
    }

    public int[] findVerticalSeam() {
        return find(true);
    }

    private int[] find(boolean vertical) {
        if ((vertical && transposed)
                || (!vertical && !transposed)) {
            transpose();
        }
        double[][] distances = new double[currentWidth][];
        for (int i = 0; i < distances.length; i++) {
            distances[i] = new double[currentHeight];
            for (int j = 0; j < distances[i].length; j++) {
                distances[i][j] = j == 0 ? energies[i][j] : Double.POSITIVE_INFINITY;
            }
        }
        int[] paths = new int[currentHeight * currentWidth];

        for (int y = 0; y < currentHeight; y++) {
            for (int x = 0; x < currentWidth; x++) {
                relax(x, y, paths, distances);
            }
        }

        double minDistance = Double.POSITIVE_INFINITY;
        int minIndex = 0;
        for (int i = 0; i < currentWidth; i++) {
            if (distances[i][currentHeight - 1] < minDistance) {
                minDistance = distances[i][currentHeight - 1];
                minIndex = i;
            }
        }
        minIndex = (currentHeight - 1) * currentWidth + minIndex;

        int[] result = new int[currentHeight];
        int i = result.length - 1;
        while (i >= 0) {
            result[i] = minIndex % currentWidth;
            minIndex = paths[minIndex];
            i--;
        }
        return result;
    }

    private void relax(int x, int y, int[] paths, double[][] distances) {
        process(x, y, x - 1, y + 1, paths, distances);
        process(x, y, x + 1, y + 1, paths, distances);
        process(x, y, x, y + 1, paths, distances);
    }

    private void process(int x, int y, int x1, int y1, int[] paths, double[][] distances) {
        if (valid(x1, y1)
                && distances[x][y] + energies[x1][y1] < distances[x1][y1]) {
            distances[x1][y1] = distances[x][y] + energies[x1][y1];
            paths[y1 * currentWidth + x1] = y * currentWidth + x;
        }
    }


    private boolean valid(int i, int j) {
        return (i >= 0 && i < currentWidth && j >= 0 && j < currentHeight);
    }

    private void transpose() {
        Color[][] temp = new Color[currentHeight][];
        double[][] tempEnergies = new double[currentHeight][];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = new Color[currentWidth];
            tempEnergies[i] = new double[currentWidth];
            for (int j = 0; j < currentWidth; j++) {
                temp[i][j] = pixels[j][i];
                tempEnergies[i][j] = energies[j][i];
            }
        }
        pixels = temp;
        energies = tempEnergies;

        transposed = !transposed;
        int var = currentHeight;
        currentHeight = currentWidth;
        currentWidth = var;
    }

    public void removeHorizontalSeam(int[] seam) {
        remove(seam, false);
    }

    public void removeVerticalSeam(int[] seam) {
        remove(seam, true);
    }

    public static void main(String[] args) {
        SeamCarver seamCarver = new SeamCarver(new Picture(args[0]));
        seamCarver.removeVerticalSeam(new int[]{ -1, 0, 1, 2, 1, 0, 0 });
    }

    private void remove(int[] seam, boolean vertical) {
        if ((vertical && transposed)
                || (!vertical && !transposed)) {
            transpose();
        }
        if (seam == null || currentWidth <= 1 || seam.length != currentHeight) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < seam.length; i++) {
            if ((i > 0 && Math.abs(seam[i] - seam[i - 1]) > 1)
                    || seam[i] < 0
                    || seam[i] >= currentWidth) {
                throw new IllegalArgumentException();
            }
        }
        for (int i = 0; i < seam.length; i++) {
            if (seam[i] < currentWidth - 1) {
                for (int j = seam[i]; j < currentWidth - 1; j++) {
                    pixels[j][i] = pixels[j + 1][i];
                    energies[j][i] = energies[j + 1][i];
                }
            }
        }
        for (int i = 0; i < seam.length; i++) {
            energies[seam[i]][i] = energyInternal(seam[i], i);
            if (seam[i] > 0) {
                energies[seam[i] - 1][i] = energyInternal(seam[i] - 1, i);
            }
        }
        currentWidth--;
    }

    private double deltaSquarePart(int x, int y, boolean horizontal) {
        Color more;
        Color less;
        if (horizontal) {
            more = pixels[x + 1][y];
            less = pixels[x - 1][y];
        } else {
            more = pixels[x][y + 1];
            less = pixels[x][y - 1];
        }
        return (more.getRed() - less.getRed()) * (more.getRed() - less.getRed())
                + (more.getGreen() - less.getGreen()) * (more.getGreen() - less.getGreen())
                + (more.getBlue() - less.getBlue()) * (more.getBlue() - less.getBlue());
    }
}
