import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @Author Oleg Ponomarev
 * @since 21.01.2017
 */
public class Deque<Item> implements Iterable<Item> {

    private class Node<Item> {
        private Item value;
        private Node<Item> next;
        private Node<Item> previous;

        Node(Item value) {
            this.value = value;
        }
    }

    private Node<Item> first;
    private Node<Item> last;
    private int size;

    public Deque() {
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        Node<Item> node = new Node<>(item);
        node.next = first;
        if (first != null) {
            first.previous = node;
        }
        first = node;
        size++;
        if (size == 1) {
            last = first;
        }
    }

    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        Node<Item> oldLast = last;
        last = new Node<>(item);
        last.previous = oldLast;
        if (oldLast != null) {
            oldLast.next = last;
        }
        size++;
        if (size == 1) {
            first = last;
        }
    }

    public Item removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item result = first.value;
        first.value = null;
        first = first.next;
        size--;
        if (size == 0) {
            last = null;
        }
        return result;
    }

    public Item removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item result = last.value;
        last.value = null;
        last = last.previous;
        if (last != null) {
            last.next = null;
        }
        size--;
        if (size == 0) {
            first = null;
        }
        return result;
    }

    @Override
    public Iterator<Item> iterator() {
        return new Iterator<Item>() {
            private Node<Item> current = first;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Item next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                Item result = current.value;
                current = current.next;
                return result;
            }
        };
    }
}
