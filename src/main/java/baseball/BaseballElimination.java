package baseball;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BaseballElimination {
    private final Map<String, Integer> teams;
    private final int[] wins;
    private final int[] losses;
    private final int[] remaining;
    private final int[][] games;

    public BaseballElimination(String filename) {
        In in = new In(filename);
        int number = in.readInt();
        teams = new HashMap<>();
        wins = new int[number];
        losses = new int[number];
        remaining = new int[number];
        games = new int[number][number];
        for (int i = 0; i < number; i++) {
            teams.put(in.readString(), i);
            wins[i] = in.readInt();
            losses[i] = in.readInt();
            remaining[i] = in.readInt();
            for (int j = 0; j < number; j++) {
                games[i][j] = in.readInt();
            }
        }
    }

    private int pairs(int n) {
        return n * (n - 1) / 2;
    }

    public int numberOfTeams() {
        return teams.size();
    }

    public Iterable<String> teams() {
        return teams.keySet();
    }

    private void validateTeam(String team) {
        if (team == null || teams.get(team) == null) {
            throw new IllegalArgumentException();
        }
    }

    public int wins(String team) {
        validateTeam(team);
        return wins[teams.get(team)];
    }

    public int losses(String team) {
        validateTeam(team);
        return losses[teams.get(team)];
    }

    public int remaining(String team) {
        validateTeam(team);
        return remaining[teams.get(team)];
    }

    public int against(String team1, String team2) {
        validateTeam(team1);
        validateTeam(team2);
        return games[teams.get(team1)][teams.get(team2)];
    }

    private Iterable<Integer> dummyTest(int x) {
        Bag<Integer> bag = new Bag<>();
        for (int i = 0; i < teams.size(); i++) {
            if (wins[x] + remaining[x] < wins[i]) {
                bag.add(i);
            }
        }
        return bag;
    }

    public boolean isEliminated(String team) {
        validateTeam(team);
        int x = teams.get(team);
        if (dummyTest(x).iterator().hasNext()) {
            return true;
        }

        int totalGamesRemaining = getTotalGamesRemaining(x);
        int pairs = pairs(teams.size() - 1);
        Map<Integer, Integer> teamToVertex = getTeamToVertexMap(x, pairs);
        FordFulkerson fordFulkerson = compute(x, teamToVertex, pairs);
        return Math.abs(fordFulkerson.value() - totalGamesRemaining) > 0.01;
    }

    private Map<Integer, Integer> getTeamToVertexMap(int x, int pairs) {
        Map<Integer, Integer> teamToVertex = new HashMap<>();
        int number = pairs;
        for (Integer teamNumber : teams.values()) {
            if (teamNumber == x) {
                continue;
            }
            teamToVertex.put(teamNumber, ++number);
        }
        return teamToVertex;
    }

    private int getTotalGamesRemaining(int x) {
        int totalGamesRemaining = 0;
        for (int i = 0; i < games.length; i++) {
            for (int j = i + 1; j < games[i].length; j++) {
                if (i != x && j != x) {
                    totalGamesRemaining += games[i][j];
                }
            }
        }
        return totalGamesRemaining;
    }

    private FordFulkerson compute(int x, Map<Integer, Integer> teamToVertex, int pairs) {
        FlowNetwork flowNetwork = new FlowNetwork(1 + pairs + (teams.size() - 1) + 1);
        int count = 1;
        for (int i = 0; i < games.length; i++) {
            if (i == x) {
                continue;
            }
            for (int j = i + 1; j < games[i].length; j++) {
                if (j == x) {
                    continue;
                }
                flowNetwork.addEdge(new FlowEdge(0, count, games[i][j]));
                flowNetwork.addEdge(new FlowEdge(count, teamToVertex.get(i), Double.POSITIVE_INFINITY));
                if (!isConnectedToTarget(flowNetwork, teamToVertex.get(i))) {
                    flowNetwork.addEdge(new FlowEdge(
                            teamToVertex.get(i), flowNetwork.V() - 1, wins[x] + remaining[x] - wins[i]));
                }
                flowNetwork.addEdge(new FlowEdge(count, teamToVertex.get(j), Double.POSITIVE_INFINITY));
                if (!isConnectedToTarget(flowNetwork, teamToVertex.get(j))) {
                    flowNetwork.addEdge(new FlowEdge(
                            teamToVertex.get(j), flowNetwork.V() - 1, wins[x] + remaining[x] - wins[j]));
                }
                count++;
            }
        }
        return new FordFulkerson(flowNetwork, 0, flowNetwork.V() - 1);
    }

    private boolean isConnectedToTarget(FlowNetwork flowNetwork, int i) {
        boolean connected = false;
        for (FlowEdge edge : flowNetwork.adj(i)) {
            if (edge.to() == flowNetwork.V() - 1) {
                connected = true;
                break;
            }
        }
        return connected;
    }

    public Iterable<String> certificateOfElimination(String team) {
        validateTeam(team);
        int x = teams.get(team);
        final Iterator<Integer> iterator = dummyTest(x).iterator();
        Bag<String> subset = new Bag<>();
        while (iterator.hasNext()) {
            final Integer next = iterator.next();
            for (Map.Entry<String, Integer> entryTeam : teams.entrySet()) {
                if (entryTeam.getValue().equals(next)) {
                    subset.add(entryTeam.getKey());
                    break;
                }
            }
        }
        if (!subset.isEmpty()) {
            return subset;
        }
        int totalGamesRemaining = getTotalGamesRemaining(x);
        int pairs = pairs(teams.size() - 1);
        Map<Integer, Integer> teamToVertex = getTeamToVertexMap(x, pairs);
        FordFulkerson fordFulkerson = compute(x, teamToVertex, pairs);
        if (Math.abs(fordFulkerson.value() - totalGamesRemaining) < 0.01) {
            return null;
        }
        for (Map.Entry<Integer, Integer> entry : teamToVertex.entrySet()) {
            if (fordFulkerson.inCut(entry.getValue())) {
                for (Map.Entry<String, Integer> entryTeam : teams.entrySet()) {
                    if (entryTeam.getValue().equals(entry.getKey())) {
                        subset.add(entryTeam.getKey());
                        break;
                    }
                }
            }
        }
        return subset;
    }

    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            } else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}
