import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    private static int[] cache;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int coins[] = new int[m];
        for(int coins_i=0; coins_i < m; coins_i++){
            coins[coins_i] = in.nextInt();
        }
        cache = new int[n + 1];
        for (int i = 0; i < cache.length; i++) {
            cache[i] = -1;
        }
        System.out.println(count(n, coins, 0));
        System.out.println(Arrays.toString(cache));
    }

    private static int count(int sum, int[] coins, int index) {
        if (sum < 0 || index >= coins.length) {
            return 0;
        } else if (sum == 0) {
            return 1;
        } else if (cache[sum] != -1) {
            return cache[sum];
        } else {
            cache[sum] = count(sum - coins[index], coins, index) + count(sum, coins, index + 1);
            return cache[sum];
        }
    }
}
