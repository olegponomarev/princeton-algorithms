import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @Author Oleg Ponomarev
 * @since 21.01.2017
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] items;
    private int size;

    public RandomizedQueue() {
        items = (Item[]) new Object[1];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (size == items.length) {
            items = resize(items, items.length * 2);
        }
        int index = 0;
        while (items[index] != null) {
            index++;
        }
        items[index] = item;
        size++;
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int index = StdRandom.uniform(items.length);
        while (items[index] == null) {
            index = StdRandom.uniform(items.length);
        }
        Item result = items[index];
        items[index] = null;
        size--;
        if (size <= items.length / 4 && items.length > 1) {
            items = resize(items, items.length / 2);
        }
        return result;
    }

    private Item[] resize(Item[] arrayToResize, int newSize) {
        Item[] newItems = (Item[]) new Object[newSize];
        int index = 0;
        for (Item item : arrayToResize) {
            if (item != null) {
                newItems[index++] = item;
            }
        }
        return newItems;
    }

    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int index = StdRandom.uniform(items.length);
        while (items[index] == null) {
            index = StdRandom.uniform(items.length);
        }
        return items[index];
    }

    @Override
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private class RandomIterator implements Iterator<Item> {
        private int[] indexesOfItems = new int[size];
        private boolean[] seenItems = new boolean[size];
        private int notSeenCount = size;

        public RandomIterator() {
            int index = 0;
            for (int i = 0; i < items.length; i++) {
                if (items[i] != null) {
                    indexesOfItems[index++] = i;
                }
            }
        }

        @Override
        public boolean hasNext() {
            return notSeenCount > 0;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            int index = StdRandom.uniform(size);
            while (seenItems[index]) {
                index = StdRandom.uniform(size);
            }
            seenItems[index] = true;
            notSeenCount--;
            return items[indexesOfItems[index]];
        }
    }
}
