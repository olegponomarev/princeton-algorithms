package boggle;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.TST;

import java.util.Set;
import java.util.TreeSet;

public class BoggleSolver {
    private final TST<Integer> dictionary = new TST<>();

    public BoggleSolver(String[] dictionary) {
        for (String s : dictionary) {
            this.dictionary.put(s, 1);
        }
    }

    public Iterable<String> getAllValidWords(BoggleBoard board) {
        Set<String> set = new TreeSet<>();
        for (int i = 0; i < board.rows(); i++) {
            for (int j = 0; j < board.cols(); j++) {
                final boolean[][] used = new boolean[board.rows()][board.cols()];
                used[i][j] = true;
                dfs(i, j, board, letter(board.getLetter(i, j)), set, used);
            }
        }
        return set;
    }

    private String letter(char letter) {
        return letter == 'Q' ? "QU" : String.valueOf(letter);
    }

    private void dfs(int i, int j, BoggleBoard board, String prefix, Set<String> set, boolean[][] used) {
        /* if (prefix.equals("Y") || prefix.equals("YO") || prefix.equals("YOU")) {
            System.out.println(prefix);
        } */
        if (!dictionary.keysWithPrefix(prefix).iterator().hasNext()) {
            return;
        }
        if (dictionary.contains(prefix) && prefix.length() > 2) {
            set.add(prefix);
        }
        for (int k = i - 1; k <= i + 1; k++) {
            for (int m = j - 1; m <= j + 1; m++) {
                if ((k != i || m != j) && valid(k, m, board) && !used[k][m]) {
                    final boolean[][] copy = new boolean[used.length][used[0].length];
                    for (int index = 0; index < copy.length; index++) {
                        System.arraycopy(used[index], 0, copy[index], 0, used[index].length);
                    }
                    copy[k][m] = true;
                    dfs(k, m, board, prefix + letter(board.getLetter(k, m)), set, copy);
                }
            }
        }
    }

    private boolean valid(int i, int j, BoggleBoard board) {
        return i >= 0 && i < board.rows() && j >= 0 && j < board.cols();
    }

    public int scoreOf(String word) {
        if (!dictionary.contains(word)) {
            return 0;
        }
        if (word.length() < 3) {
            return 0;
        } else if (word.length() < 5) {
            return 1;
        } else if (word.length() == 5) {
            return 2;
        } else if (word.length() == 6) {
            return 3;
        } else if (word.length() == 7) {
            return 5;
        } else {
            return 11;
        }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
    }
}
