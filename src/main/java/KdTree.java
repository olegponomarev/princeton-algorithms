import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.awt.Color;


/**
 * @author Oleg Ponomarev
 * @since 2/22/17
 */
public class KdTree {
    private static final double BIAS = 0.00000001;
    private Node root;
    private int size;

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void insert(Point2D p) {
        if (root == null) {
            root = new Node(p, true);
            size++;
            return;
        }
        insert(root, p, true);
    }

    private void insert(Node node, Point2D point, boolean isEvenLevel) {
        if (node.value.equals(point)) {
            return;
        }
        if (isEvenLevel) {
            if (point.x() - node.value.x() < BIAS) {
                if (node.left != null) {
                    insert(node.left, point, !isEvenLevel);
                } else {
                    node.left = new Node(point, !isEvenLevel);
                    size++;
                }
            } else {
                if (node.right != null) {
                    insert(node.right, point, !isEvenLevel);
                } else {
                    node.right = new Node(point, !isEvenLevel);
                    size++;
                }
            }
        } else {
            if (point.y() - node.value.y() < BIAS) {
                if (node.left != null) {
                    insert(node.left, point, !isEvenLevel);
                } else {
                    node.left = new Node(point, !isEvenLevel);
                    size++;
                }
            } else {
                if (node.right != null) {
                    insert(node.right, point, !isEvenLevel);
                } else {
                    node.right = new Node(point, !isEvenLevel);
                    size++;
                }
            }
        }
    }

    public boolean contains(Point2D p) {
        Node current = root;
        while (current != null) {
            if (current.value.equals(p)) {
                return true;
            }
            if (current.isEvenLevel) {
                if (p.x() - current.value.x() < BIAS) {
                    if (current.left != null) {
                        current = current.left;
                    } else {
                        return false;
                    }
                } else {
                    if (current.right != null) {
                        current = current.right;
                    } else {
                        return false;
                    }
                }
            } else {
                if (p.y() - current.value.y() < BIAS) {
                    if (current.left != null) {
                        current = current.left;
                    } else {
                        return false;
                    }
                } else {
                    if (current.right != null) {
                        current = current.right;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public void draw() {
        draw(root, Color.RED, 0, 1);
    }

    private void draw(Node node, Color color, double lowerLimit, double upperLimit) {
        if (node == null) {
            return;
        }
        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(Color.BLACK);
        node.value.draw();
        StdDraw.setPenColor(color);
        StdDraw.setPenRadius();
        double interFace = -1;
        if (color.equals(Color.RED)) {
            StdDraw.line(node.value.x(), lowerLimit, node.value.x(), upperLimit);
            color = Color.BLUE;
            interFace = node.value.x();
        } else if (color.equals(Color.BLUE)) {
            StdDraw.line(lowerLimit, node.value.y(), upperLimit, node.value.y());
            color = Color.RED;
            interFace = node.value.y();
        }
        if (interFace == -1) {
            throw new RuntimeException();
        }
        draw(node.left, color, 0, interFace);
        draw(node.right, color, interFace, 1);
    }

    public Iterable<Point2D> range(RectHV rect) {
        Queue<Point2D> queue = new Queue<>();
        checkNode(root, rect, queue);
        return queue;
    }

    private void checkNode(Node node, RectHV rect, Queue<Point2D> queue) {
        if (node == null) {
            return;
        }
        if (rect.contains(node.value)) {
            queue.enqueue(node.value);
        }
        if (node.isEvenLevel) {
            if (rect.xmin() - node.value.x() < BIAS) {
                checkNode(node.left, rect, queue);
            }
            if (rect.xmax() - node.value.x() > BIAS) {
                checkNode(node.right, rect, queue);
            }
        } else {
            if (rect.ymin() - node.value.y() < BIAS) {
                checkNode(node.left, rect, queue);
            }
            if (rect.ymax() - node.value.y() > BIAS) {
                checkNode(node.right, rect, queue);
            }
        }
    }

    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
//        if (root == null) {
//            return null;
//        }
//        if (root.value.equals(p)) {
//            return root.value;
//        }
        return nearest(root, p);
    }

//    It's not optimized. See lectures to remember how to do it better
    private Point2D nearest(Node node, Point2D p) {
        if (node == null) {
            return null;
        }
        if (node.value.equals(p)) {
            return node.value;
        }
        Point2D nearestLeft = nearest(node.left, p);
        Point2D nearestRight = nearest(node.right, p);

        if (nearestLeft == null || nearestLeft.distanceTo(p) - node.value.distanceTo(p) > BIAS) {
            nearestLeft = node.value;
        }
        if (nearestRight == null || nearestRight.distanceTo(p) - node.value.distanceTo(p) > BIAS) {
            nearestRight = node.value;
        }
        if (nearestLeft.distanceTo(p) - nearestRight.distanceTo(p) < BIAS) {
            return nearestLeft;
        } else {
            return nearestRight;
        }
    }

    private class Node {
        private Point2D value;
        private Node left;
        private Node right;
        private boolean isEvenLevel;

        Node(Point2D value, boolean isEvenLevel) {
            this.value = value;
            this.isEvenLevel = isEvenLevel;
        }
    }

}
