import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Oleg Ponomarev
 * @since 23.01.2017
 */
public class BruteCollinearPoints {
    private static final double PRECISION = 0.001;
    private List<LineSegment> lineSegmentList = new ArrayList<>();

    public BruteCollinearPoints(Point[] points) {
        if (points == null) {
            throw new NullPointerException();
        }
        for (Point point : points) {
            if (point == null) {
                throw new NullPointerException();
            }
        }
        Arrays.sort(points);
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].compareTo(points[j]) == 0) {
                    throw new IllegalArgumentException();
                }
            }
        }
//        Arrays.sort(points, new Point(0, 0).slopeOrder());
//        List<Double> slopeOrders = new ArrayList<>();
//        for (Point point : points) {
//            slopeOrders.add(points[0].slopeTo(point));
//        }
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                NEXT:
                for (int k = j + 1; k < points.length; k++) {
                    if (Math.abs(points[i].slopeTo(points[j]) - points[j].slopeTo(points[k])) < PRECISION) {
                        for (int m = k + 1; m < points.length; m++) {
                            if (Math.abs(points[k].slopeTo(points[m]) - points[j].slopeTo(points[k])) < PRECISION) {
                                lineSegmentList.add(new LineSegment(points[i], points[m]));
                                break NEXT;
                            }
                        }
                    }
                }
            }
        }
    }

    public int numberOfSegments() {
        return lineSegmentList.size();
    }

    public LineSegment[] segments() {
        LineSegment[] array = new LineSegment[numberOfSegments()];
        return lineSegmentList.toArray(array);
    }
}
